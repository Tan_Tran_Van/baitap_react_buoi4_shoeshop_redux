import React, { Component } from "react";
import { connect } from "react-redux";
import { HANDLE_ADD_SHOE_TO_CART } from "./redux/constants/constant_ShoeShop";

class ModalDetailShoe extends Component {
  render() {
    let { name, price, description, quantity, image } = this.props.detailShoe;
    return (
      <div
        className="modal fade"
        id="modalDetailShoe"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document" style={{ minWidth: 800 }}>
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Thông Tin Chi Tiết Sản Phẩm</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              {/* Detail Shoe */}
              <div className="row">
                <div className="col-4">
                  <img className="img-fluid" src={image} alt={image} />
                </div>
                <div className="col-8 text-left">
                  <h3>{name}</h3>
                  <p>{description}</p>
                  <h3>$ {price}</h3>
                  <p>Số Lượng: {quantity}</p>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-warning"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                className="btn btn-danger"
                onClick={() => {
                  this.props.handelAddShoeToCart(this.props.detailShoe);
                }}
              >
                Add To Cart
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detailShoe: state.ShoeShopReducer.detailShoe,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handelAddShoeToCart: (choseShoe) => {
      let action = {
        type: HANDLE_ADD_SHOE_TO_CART,
        payload: choseShoe,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ModalDetailShoe);
