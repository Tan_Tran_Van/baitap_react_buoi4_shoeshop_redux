import React, { Component } from "react";
import { connect } from "react-redux";

class HeaderCart extends Component {
  render() {
    let tongSoLuong = this.props.shoeCart.reduce((tongSL, shoe, index) => {
      return (tongSL += shoe.soLuong);
    }, 0);
    return (
      <div className="row bg-light justify-content-between py-3">
        <h2 className="col-6 text-left">SHOE SHOP TAN TAN</h2>
        <div className="col-6 text-right">
          <button
            className="btn btn-success"
            data-toggle="modal"
            data-target="#modalShoeCart"
          >
            <i className="fa fa-cart-plus" /> ({tongSoLuong})
          </button>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    shoeCart: state.ShoeShopReducer.shoeCart,
  };
};

export default connect(mapStateToProps, null)(HeaderCart);
