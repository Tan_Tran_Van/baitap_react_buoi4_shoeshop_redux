import React, { Component } from "react";
import ListShoe from "./ListShoe";
import ModalDetailShoe from "./ModalDetailShoe";
import ModalShoeCart from "./ModalShoeCart";
import HeaderCart from "./HeaderCart";

export default class ShoeShopRedux extends Component {
  render() {
    return (
      <div className="container-fluid px-5">
        <HeaderCart />
        <ModalShoeCart />
        <ListShoe />
        <ModalDetailShoe />
      </div>
    );
  }
}
