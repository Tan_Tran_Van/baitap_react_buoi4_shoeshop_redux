import React, { Component } from "react";
import { connect } from "react-redux";
import Cart from "./Cart";
import { HANDLE_DELETE_ALL_CART } from "./redux/constants/constant_ShoeShop";

class ModalShoeCart extends Component {
  render() {
    return (
      <div
        className="modal fade"
        id="modalShoeCart"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document" style={{ minWidth: 900 }}>
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Thông Tin Chi Tiết Giỏ Hàng</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <Cart />
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-warning"
                data-dismiss="modal"
                onClick={() => {
                  this.props.handleDeleteAllCart();
                }}
              >
                Xoá Giỏ Hàng
              </button>
              <button
                className="btn btn-danger"
                onClick={() => {
                  this.props.handleDeleteAllCart();
                }}
              >
                Thanh Toán
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleDeleteAllCart: () => {
      let action = {
        type: HANDLE_DELETE_ALL_CART,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ModalShoeCart);
