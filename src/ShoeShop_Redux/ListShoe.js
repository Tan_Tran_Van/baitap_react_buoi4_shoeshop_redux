import React, { Component } from "react";
import { connect } from "react-redux";
import {
  HANDLE_ADD_SHOE_TO_CART,
  HANDLE_CHOSE_DETAIL_SHOE,
} from "./redux/constants/constant_ShoeShop";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.dataShoe?.map((item, index) => {
      return (
        <div className="col-4 p-2" key={index}>
          <div className=" card text-left">
            <img className="card-img-top" src={item.image} alt={item.image} />
            <div className="card-body">
              <h4 className="card-title">{item.name}</h4>
              <h3 className="card-text font-weight-bold">$ {item.price}</h3>
              <p className="card-text">{item.shortDescription}</p>
            </div>
            <div className="cardr-footer p-1">
              <div className="row">
                <button
                  className="btn btn-warning offset-1 col-4"
                  data-toggle="modal"
                  data-target="#modalDetailShoe"
                  onClick={() => {
                    this.props.handleChoseDetailShoe(item);
                  }}
                >
                  Xem Chi Tiết
                </button>
                <button
                  className="btn btn-danger offset-2 col-4"
                  onClick={() => {
                    this.props.handelAddShoeToCart(item);
                  }}
                >
                  Add To Cart
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    });
  };
  render() {
    return (
      <div className="container py-2">
        <div className="row">{this.renderListShoe()}</div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    dataShoe: state.ShoeShopReducer.dataShoe,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleChoseDetailShoe: (detailShoe) => {
      let action = {
        type: HANDLE_CHOSE_DETAIL_SHOE,
        payload: detailShoe,
      };
      dispatch(action);
    },
    handelAddShoeToCart: (choseShoe) => {
      let action = {
        type: HANDLE_ADD_SHOE_TO_CART,
        payload: choseShoe,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListShoe);
