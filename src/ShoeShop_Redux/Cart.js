import React, { Component } from "react";
import { connect } from "react-redux";
import {
  HANDLE_DELETE_SHOE_IN_CART,
  HANDLE_UP_DOWN_QUANTITY,
} from "./redux/constants/constant_ShoeShop";

class Cart extends Component {
  renderdataCart = () => {
    return this.props.dataCart?.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt={item.image} />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              className="btn font-weight-bold"
              onClick={() => {
                this.props.handleUpDownQuantity(index, -1);
              }}
            >
              -
            </button>
            <span className="px-2">{item.soLuong}</span>
            <button
              className="btn font-weight-bold"
              onClick={() => {
                this.props.handleUpDownQuantity(index, +1);
              }}
            >
              +
            </button>
          </td>
          <td>$ {item.price}</td>
          <td className="font-weight-bold">
            $ {(item.price * item.soLuong).toLocaleString()}
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleDeleteShoeInCart(index);
              }}
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    let tongTien = this.props.dataCart.reduce((tongTien, item, index) => {
      return (tongTien += item.price * item.soLuong);
    }, 0);
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>IMAGE</th>
              <th>NAME</th>
              <th>QUANTITY</th>
              <th>PRICE</th>
              <th>TOTAL</th>
              <th>SETUP</th>
            </tr>
          </thead>
          <tbody>{this.renderdataCart()}</tbody>
          <tfoot>
            <tr>
              <th colSpan={5} className="text-right">
                Tổng Tiền:
              </th>
              <th colSpan={2} className="text-left">
                $ {tongTien.toLocaleString()} USD
              </th>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    dataCart: state.ShoeShopReducer.shoeCart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleUpDownQuantity: (indexShoe, number) => {
      let action = {
        type: HANDLE_UP_DOWN_QUANTITY,
        payload: {
          indexShoe: indexShoe,
          numberUpDown: number,
        },
      };
      dispatch(action);
    },
    handleDeleteShoeInCart: (indexShoe) => {
      let action = {
        type: HANDLE_DELETE_SHOE_IN_CART,
        payload: indexShoe,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
