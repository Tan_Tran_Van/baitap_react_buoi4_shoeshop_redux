import { combineReducers } from "redux";
import { ShoeShopReducer } from "./ShoeShopReducer";

export const rootReducer_ShoeShop = combineReducers({ ShoeShopReducer });
