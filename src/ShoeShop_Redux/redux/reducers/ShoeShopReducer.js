import { dataShoe } from "../../dataShoe";
import {
  HANDLE_ADD_SHOE_TO_CART,
  HANDLE_CHOSE_DETAIL_SHOE,
  HANDLE_DELETE_ALL_CART,
  HANDLE_DELETE_SHOE_IN_CART,
  HANDLE_UP_DOWN_QUANTITY,
} from "../constants/constant_ShoeShop";

let initialState = {
  dataShoe: dataShoe,
  shoeCart: [],
  detailShoe: {},
};
export const ShoeShopReducer = (state = initialState, action) => {
  switch (action.type) {
    case HANDLE_CHOSE_DETAIL_SHOE: {
      return { ...state, detailShoe: action.payload };
    }
    case HANDLE_ADD_SHOE_TO_CART: {
      let cloneCart = [...state.shoeCart];
      let indexShoe = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (indexShoe === -1) {
        cloneCart.push({ ...action.payload, soLuong: 1 });
      } else {
        cloneCart[indexShoe].soLuong++;
      }
      return { ...state, shoeCart: cloneCart };
    }
    case HANDLE_UP_DOWN_QUANTITY: {
      let index = action.payload.indexShoe;
      let cloneCart = [...state.shoeCart];
      cloneCart[index].soLuong += action.payload.numberUpDown;
      cloneCart[index].soLuong === 0 && cloneCart.splice(index, 1);
      return { ...state, shoeCart: cloneCart };
    }
    case HANDLE_DELETE_SHOE_IN_CART: {
      let cloneCart = [...state.shoeCart];
      cloneCart.splice(action.payload, 1);
      return { ...state, shoeCart: cloneCart };
    }
    case HANDLE_DELETE_ALL_CART: {
      //   state.shoeCart = [];
      return { ...state, shoeCart: [] };
    }
    default:
      return { ...state };
  }
};
