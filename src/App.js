import "./App.css";
// import BT_Redux_ShoeShop from "./BT_Redux_ShoeShop/BT_Redux_ShoeShop";
import ShoeShopRedux from "./ShoeShop_Redux/ShoeShopRedux";

function App() {
  return (
    <div className="App">
      {/* <BT_Redux_ShoeShop /> */}
      <ShoeShopRedux />
    </div>
  );
}

export default App;
