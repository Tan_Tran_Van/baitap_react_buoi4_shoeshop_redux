import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
//setup redux, react-redux
import { Provider } from "react-redux";
import { createStore } from "redux";
// import { rootReducer_BTShoeShop } from "./BT_Redux_ShoeShop/redux/reducers/rootReducers";
import { rootReducer_ShoeShop } from "./ShoeShop_Redux/redux/reducers/rootReducer_ShoeShop";
const store = createStore(
  rootReducer_ShoeShop,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // <React.StrictMode>
  <Provider store={store}>
    <App />
  </Provider>
  // </React.StrictMode>
);

reportWebVitals();
