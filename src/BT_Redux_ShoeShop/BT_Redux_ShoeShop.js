import React, { Component } from "react";
import { connect } from "react-redux";
import ListShoeRedux from "./ListShoeRedux";
import ModalCartRedux from "./ModalCartRedux";
import ModalDetailShoeRedux from "./ModalDetailShoeRedux";

class BT_Redux_ShoeShop extends Component {
  render() {
    let tongSoLuong = this.props.shoeCart.reduce((tongSP, item, index) => {
      return (tongSP += item.soLuong);
    }, 0);
    return (
      <div className="container p-3">
        <div className="text-right">
          <button
            className="btn btn-success"
            data-toggle="modal"
            data-target="#modelCart"
          >
            CART({tongSoLuong})
          </button>
        </div>
        <ModalCartRedux />
        <ListShoeRedux />
        <ModalDetailShoeRedux />
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    shoeCart: state.BTShoeReducer.shoeCart,
  };
};
export default connect(mapStateToProps, null)(BT_Redux_ShoeShop);
