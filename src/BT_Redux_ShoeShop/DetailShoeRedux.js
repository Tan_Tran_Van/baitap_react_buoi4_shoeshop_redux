import React, { Component } from "react";
// import { connect } from "react-redux";

export default class DetailShoeRedux extends Component {
  render() {
    let { id, image, name, price, description } = this.props.detailShoe;
    return (
      <div className="row alert-secondary p-5 text-left">
        <img src={image} alt="" className="col-3" />
        <div className="col-9">
          <p>ID: {id}</p>
          <p>Name: {name}</p>
          <p>Price: $ {price}</p>
          <p>Description: {description}</p>
        </div>
      </div>
    );
  }
}

// let mapStateToProps = (state) => {
//   return {
//     detailShoe: state.BTShoeReducer.detailShoe,
//   };
// };

// export default connect(mapStateToProps, null)(DetailShoeRedux);
