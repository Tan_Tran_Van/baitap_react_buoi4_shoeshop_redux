import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoeRedux from "./ItemShoeRedux";

class ListShoeRedux extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((itemShoe, index) => {
      return <ItemShoeRedux key={index} product={itemShoe} />;
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    shoeArr: state.BTShoeReducer.shoeArr,
  };
};
export default connect(mapStateToProps, null)(ListShoeRedux);
