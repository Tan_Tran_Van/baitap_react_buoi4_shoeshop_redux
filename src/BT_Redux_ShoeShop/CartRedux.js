import React, { Component } from "react";
import { connect } from "react-redux";
import {
  HANDLE_DELETE_SHOE_IN_CART,
  HANDLE_UP_DOWN_SOLUONG,
} from "./redux/constants/handleConstants";

class CartRedux extends Component {
  renderTbody = () => {
    return this.props.shoeCart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>
            <img style={{ width: "50px" }} src={item.image} alt="" />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              className="btn font-weight-bold"
              onClick={() => {
                this.props.handleUpDownSoLuong(index, false);
              }}
            >
              -
            </button>
            <span className="p-2">{item.soLuong}</span>
            <button
              className="btn font-weight-bold"
              onClick={() => {
                this.props.handleUpDownSoLuong(index, true);
              }}
            >
              +
            </button>
          </td>
          <td>$ {item.price}</td>
          <td>$ {(item.price * item.soLuong).toLocaleString()}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleDeleteShoeInCart(index);
              }}
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    let tongTien = this.props.shoeCart.reduce((tongTien, item, index) => {
      return (tongTien += item.soLuong * item.price);
    }, 0);
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <td>Id</td>
              <td>Hình Ảnh</td>
              <td>Tên Sản Phẩm</td>
              <td>Số Lượng</td>
              <td>Đơn giá</td>
              <td>Thành Tiền</td>
              <td></td>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
          <tfoot>
            <tr>
              <td colSpan="5" className="text-right">
                Tổng Tiền:{" "}
              </td>
              <td>$ {tongTien.toLocaleString()}</td>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoeCart: state.BTShoeReducer.shoeCart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleDeleteShoeInCart: (index) => {
      let action = {
        type: HANDLE_DELETE_SHOE_IN_CART,
        index,
      };
      dispatch(action);
    },
    handleUpDownSoLuong: (index, upDown) => {
      let action = {
        type: HANDLE_UP_DOWN_SOLUONG,
        index,
        upDown,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartRedux);
