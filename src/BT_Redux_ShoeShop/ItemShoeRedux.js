import React, { Component } from "react";
import { connect } from "react-redux";
import {
  HANDLE_ADD_SHOE_TO_CART,
  HANDLE_CHOSE_DETAIL_SHOE,
} from "./redux/constants/handleConstants";

class ItemShoeRedux extends Component {
  render() {
    let { image, name, price, description } = this.props.product;
    return (
      <div className="col-3 p-2">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt={image} />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <h3 className="card-text">$ {price}</h3>
            <p className="card-text">
              {description.length > 50
                ? description.substr(0, 50) + "..."
                : description}
            </p>
          </div>
          <div>
            <button
              className="btn btn-warning mx-2"
              data-toggle="modal"
              data-target="#modelId"
              onClick={() => {
                this.props.handleChoseDetailShoe(this.props.product);
              }}
            >
              Xem Chi Tiết
            </button>
            <button
              className="btn btn-danger mx-2"
              onClick={() => {
                this.props.handleAddShoeToCart(this.props.product);
              }}
            >
              Add to Cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleChoseDetailShoe: (product) => {
      // let updateDetailShoe = product;
      let action = {
        type: HANDLE_CHOSE_DETAIL_SHOE,
        updateDetailShoe: product,
      };
      dispatch(action);
    },
    handleAddShoeToCart: (product) => {
      // let updateCart = { ...product, soLuong: 1 };
      let action = {
        type: HANDLE_ADD_SHOE_TO_CART,
        updateCart: product,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoeRedux);
