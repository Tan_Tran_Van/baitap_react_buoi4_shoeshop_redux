import React, { Component } from "react";
import { connect } from "react-redux";
import DetailShoeRedux from "./DetailShoeRedux";
import { HANDLE_ADD_SHOE_TO_CART } from "./redux/constants/handleConstants";

class ModalDetailShoeRedux extends Component {
  render() {
    // let { id, image, name, price, description } = this.props.detailShoe;
    return (
      <div>
        <div
          className="modal fade"
          id="modelId"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div
            className="modal-dialog"
            role="document"
            style={{ minWidth: "1000px" }}
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Chi Tiết Sản Phẩm</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <DetailShoeRedux detailShoe={this.props.detailShoe} />
                {/* <div className="row alert-secondary p-5 text-left">
                  <img src={image} alt="" className="col-3" />
                  <div className="col-9">
                    <p>ID: {id}</p>
                    <p>Name: {name}</p>
                    <p>Price: $ {price}</p>
                    <p>Description: {description}</p>
                  </div>
                </div> */}
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  className="btn btn-danger mx-2"
                  data-dismiss="modal"
                  onClick={() => {
                    this.props.handleAddShoeToCart(this.props.detailShoe);
                  }}
                >
                  Add to Cart
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detailShoe: state.BTShoeReducer.detailShoe,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddShoeToCart: (detailShoe) => {
      let updateCart = { ...detailShoe, soLuong: 1 };
      let action = {
        type: HANDLE_ADD_SHOE_TO_CART,
        updateCart,
      };
      dispatch(action);
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalDetailShoeRedux);
