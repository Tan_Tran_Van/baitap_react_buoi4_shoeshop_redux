import { dataShoe } from "../../dataShoe";
import {
  HANDLE_ADD_SHOE_TO_CART,
  HANDLE_CHOSE_DETAIL_SHOE,
  HANDLE_DELETE_ALL_CART,
  HANDLE_DELETE_SHOE_IN_CART,
  HANDLE_UP_DOWN_SOLUONG,
} from "../constants/handleConstants";

let initialState = {
  shoeArr: dataShoe,
  detailShoe: {},
  shoeCart: [],
};

export const BTShoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case HANDLE_CHOSE_DETAIL_SHOE: {
      state.detailShoe = action.updateDetailShoe;
      //setstate
      // state.detailShoe = { ...state.detailShoe };
      // return { ...state };
      return { ...state, detailShoe: state.detailShoe };
    }
    case HANDLE_ADD_SHOE_TO_CART: {
      let index = state.shoeCart.findIndex((item) => {
        return item.id === action.updateCart.id;
      });
      if (index !== -1) {
        state.shoeCart[index].soLuong++;
      } else {
        let updateShoe = { ...action.updateCart, soLuong: 1 };
        state.shoeCart.push(updateShoe);
      }
      // state.shoeCart = [...state.shoeCart];
      // return { ...state };
      return { ...state, shoeCart: [...state.shoeCart] };
    }
    case HANDLE_DELETE_ALL_CART: {
      state.shoeCart = [];
      // state.shoeCart = [...state.shoeCart];
      // return { ...state };
      return { ...state, shoeCart: [...state.shoeCart] };
    }
    case HANDLE_DELETE_SHOE_IN_CART: {
      state.shoeCart.splice(action.index, 1);
      state.shoeCart = [...state.shoeCart];
      return { ...state };
    }
    case HANDLE_UP_DOWN_SOLUONG: {
      if (action.upDown) {
        state.shoeCart[action.index].soLuong++;
      } else {
        if (state.shoeCart[action.index].soLuong > 1) {
          state.shoeCart[action.index].soLuong--;
        }
      }
      state.shoeCart = [...state.shoeCart];
      return { ...state };
    }

    default:
      return { ...state };
  }
};
