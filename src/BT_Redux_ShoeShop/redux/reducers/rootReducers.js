import { combineReducers } from "redux";
import { BTShoeReducer } from "./BTShoeShopReducer";

export const rootReducer_BTShoeShop = combineReducers({ BTShoeReducer });
